"use strict";

const express = require("express");
const router = express.Router();
const { getCars } = require("../controllers/cars/get-cars");
const { getCarById } = require("../controllers/cars/get-car-by-id");
const { createCar } = require("../controllers/cars/create-car");
const { deleteCarById } = require("../controllers/cars/delete-car-by-id");
//const cars = require('../controllers/cars/get-cars');

//router.route('/')
//  .get((req, res) => getCars(req, res));
router.route("/").get(getCars).post(createCar);

router.route("/:idCar").get(getCarById).delete(deleteCarById);
//cars.getCars
module.exports = router;
