"use strict";

const joi = require("joi");
const { addCar } = require("../../repositories/cars-repository");

const schema = joi.object().keys({
  marca: joi.string().min(3).max(20).required(),
  modelo: joi.string().min(2).max(220).required(),
  anho: joi
    .number()
    .integer()
    .positive()
    .min(1950)
    .max(new Date().getFullYear()),
  motor: joi.string().valid("Diesel", "Gasolina", "Híbrido", "Eléctrico"),
  cv: joi.number().integer().positive().min(60).max(500),
});

async function createCar(req, res) {
  try {
    console.log(req.body);
    const { body } = req;
    await schema.validateAsync(body);
    await addCar(body);
    res.status(201);
    res.end();
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = { createCar };
