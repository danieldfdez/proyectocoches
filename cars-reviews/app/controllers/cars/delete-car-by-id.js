"use strict";

const joi = require("joi");
const {
  findCarById,
  deleteCar,
} = require("../../repositories/cars-repository");

const schema = joi.number().integer().positive().required();

async function deleteCarById(req, res) {
  try {
    const { idCar } = req.params;
    await schema.validateAsync(idCar);
    const car = await findCarById(idCar);
    if (!car) {
      throw new Error("Id no valido");
    }
    const restCars = await deleteCar(car);
    res.status(200);
    res.send(restCars);
  } catch (err) {
    console.log(err.message);
    res.status(400);
    res.send({ Error: err.message });
  }
}

module.exports = { deleteCarById };
